export const teamMembers = [
  {
    color: "var(--five-blue-color)",
    description:
      "If we told you Rahul was Batman, you probably wouldn’t believe us. Regardless of what you think, that is what we call him, although we spell it ‘Bhatman’. In his 9+ years of launching and growing small to medium-sized businesses, he has developed a taste for seeing numbers rise and makes it a priority to ensure growth in all of our clients’ businesses. Have you ever had anyone bring devil sticks to a meeting? Rahul wouldn’t either but he’d show you his moves if you asked nicely.",
    image:
      "https://res.cloudinary.com/ghostit-co/image/upload/v1544164208/rahul.jpg",
    location: "Victoria",
    name: "Rahul Bhatia",
    title: "TCB Specialist"
  },
  {
    color: "var(--five-orange-color)",
    description:
      "Having come from a long line of successful writers, journalists and editors, it shouldn't have come as a surprise for Mishar to end up a writer — but it did. She loves helping people find their voice and create a solid brand. Her many years travelling and living abroad has given her a unique voice of her own to tell stories of any nature. When she's not creating content, you might find her in the kitchen trying something new. However, she's often on the go searching for the best coffee, the best craft beer, and the best food... All the food.",
    image:
      "https://res.cloudinary.com/ghostit-co/image/upload/v1544164096/mishar.jpg",
    location: "Paris",
    name: "Mishar Briones",
    title: "SheEO"
  },
  {
    color: "var(--five-purple-color)",
    description:
      "Stephanie is an island girl, born and raised. Originally from Nanaimo, she finally settled down and decided to call Victoria, BC home. Stephanie is passionate about travel and all things creative and can often be found photographing beaches and beer along the coast thanks to her trusty RAV4, May.",
    image:
      "https://res.cloudinary.com/ghostit-co/image/upload/v1544164234/stephanie.jpg",
    location: "Victoria",
    name: "Stephanie B",
    title: "Content Manager"
  },
  {
    color: "var(--five-blue-color)",
    description:
      "Although most of her life is spent chasing after her preschool-aged daughter, Erika enjoys the time she gets to either be out living it up with great friends or at home with her nose buried in a book. When she gets a chance to sit at her computer, Erika loves researching and writing about just about everything. But it's fashion, beauty and lifestyle topics that are truly her expertise. You may actually see a novel out with her name on it someday! She sees herself as somewhat of a budding “domestic goddess” and believes all problems can be solved with a good cup of tea and large amounts of chocolate.",
    image:
      "https://res.cloudinary.com/ghostit-co/image/upload/v1544163966/erika.jpg",
    location: "Victoria",
    name: "Erika Palmer",
    title: "Content Manager"
  },
  {
    color: "var(--five-orange-color)",
    description:
      "When he’s not busy obsessing over creating ‘90s R&B playlists (yes plural, there are many) on Spotify, Patrick really likes writing. So much so that this wide-eyed whippersnapper from Salt Spring Island moved to Toronto to get a master’s degree in journalism and parlayed it into working as a sports writer (Google your boy). Now he’s back on the west coast… with a vengeance.",
    image:
      "https://res.cloudinary.com/ghostit-co/image/upload/v1544164140/patrick.jpg",
    location: "Victoria",
    name: "Patrick Cwiklinski",
    title: "Content Manager"
  },

  {
    color: "var(--five-purple-color)",
    description:
      "Tori likes short walks to the fridge and believes the Oxford comma is seriously underrated. On weekends you may find her going for hikes somewhere green, slaying at Scrabble games or writing run-on sentences in posts for her travel blog.",
    image:
      "https://res.cloudinary.com/ghostit-co/image/upload/v1544164253/tori.png",
    location: "Victoria",
    name: "Tori Dudys",
    title: "Content Manager"
  },
  {
    color: "var(--five-purple-color)",
    description:
      "Emily is captivated by storytelling in its every shape and form. Powered by lattes, Netflix binges, and the thrill of finding her next great read, Emily has written her way across Australia, England, America, and back. Nowadays, she’s a versatile writer dedicated to creatively weaving stories into the heart of every project — from blogs and news articles to social media posts and marketing campaigns.",
    image:
      "https://res.cloudinary.com/ghostit-co/image/upload/v1568659558/Emily_Kennaley.png",
    location: "toronto",
    name: "Emily Kennaley",
    title: "Content Manager"
  },
  {
    color: "var(--five-purple-color)",
    description:
      "Some call me a wizard.. I call myself the css wizard. Although I love programming I do have a social life, meet my bestfriends keyboard and mouse.",
    image:
      "https://res.cloudinary.com/ghostit-co/image/upload/v1544164187/peter.jpg",
    location: "Victoria",
    name: "Peter Mirmotahari",
    title: "CSS Wizard"
  }
];
