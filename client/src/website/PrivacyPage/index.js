import React, { Component } from "react";
import Page from "../../components/containers/Page";

class PrivacyPage extends Component {
  render() {
    return (
      <Page
        className="mx32 website-page"
        title="Pricing"
        description="Ghostit privacy policy."
        keywords="ghostit, privacy"
      >
        <h1 className="my16 tac">Privacy Policy</h1>
        <p className="my8">
          Ghostit (“us”, “we”, or “our”) operates the http://ghostit.co/ website
          (the “Service”).
        </p>
        <p className="my8">
          Personal Information Collect, Use, Disclosure and Consent
        </p>
        <p className="my8">
          Personal Information may include your name, email address, age, home
          address, phone number, marital status, income, credit history, medical
          information, education, employment information and social registration
          numbers.
        </p>
        <p className="my8">
          We are committed to providing our clients, customers, members (“you”,
          “your” or “them”) with exceptional service. Providing exceptional
          service may involve the collection, use and, at times, the disclosure
          of your Personal Information. Protecting your Personal Information is
          one of our highest priorities. While we have always respected your
          privacy and safeguarded all Personal Information, we have strengthened
          our commitment to this goal. This is to continue to provide
          exceptional service to you and to comply with all laws regarding the
          collection, use and disclosure of Personal Information. We will inform
          you of why and how we collect, use and disclose Personal Information;
          obtain your consent, as required; and handle Personal Information
          according to applicable law. Our privacy commitment includes ensuring
          the accuracy, confidentiality, and security of your Personal
          Information and allowing you to request access to, and correction of,
          your personal information.
        </p>
        <p className="my8">
          This page informs you of our policies regarding the collection, use
          and disclosure of Personal Information when you use our Service.
        </p>
        <p className="my8">
          We will not use or share your information with anyone except as
          described in this Privacy Policy.
        </p>
        <p className="my8">
          We will only collect Personal Information that is necessary to fulfill
          the following purposes:
        </p>
        <p>- To verify identity;</p>
        <p>- To identify your preferences;</p>
        <p>- To open and manage an account;</p>
        <p>
          - To ensure you receive a high standard of service;- To meet
          regulatory requirements;
        </p>
        <p>
          - Other legal reasons as apply to the goods and services requested.
        </p>
        <p className="my8">
          We will obtain your consent to collect, use or disclose Personal
          Information. In some cases, we can do so without your consent (see
          below). You can provide consent orally, in writing, electronically or
          through an authorized representative.
        </p>
        <p className="my8">
          You provide implied consent where our purpose for collecting using or
          disclosing your Personal Information would be considered obvious or
          reasonable in the circumstances. Consent may also be implied where you
          have notice and a reasonable opportunity to opt-out of having your
          Personal Information used for mail-outs, marketing or fundraising and
          you do not opt-out.
        </p>
        <p className="my8">
          We may collect, use or disclose Personal Information without the
          consent:
        </p>
        <p>- When permitted or required by law;</p>
        <p>
          - In an emergency that threatens an individual’s life, health, or
          personal security;
        </p>
        <p>
          - When the Personal Information is available from a public source;
        </p>
        <p>- When we require legal advice from a lawyer;</p>
        <p>- For the purposes of collecting a debt or protection from fraud;</p>
        <p>- Other legally established reasons.</p>
        <p className="my8">
          We will not sell your Personal Information to other parties unless
          consent has been provided or implied. We retain your Personal
          Information for the time necessary to fulfill the identified purposes
          or a legal or business purpose. We will make reasonable efforts to
          ensure that your Personal Information is accurate and complete. You
          may request correction to your Personal Information to ensure its
          accuracy and completeness. A request must be in writing and provide
          sufficient detail to identify your Personal Information and the
          correction being sought.
        </p>
        <p className="my8">
          We are committed to ensuring the security of your Personal Information
          and may use passwords, encryption, firewalls, restricted employee
          access or other methods, in our discretion. We will use appropriate
          security measures when destroying your Personal Information such as
          shredding documents or deleting electronically stored information, in
          our discretion.
        </p>
        <h4 className="my16">Log Data</h4>
        <p className="my8">
          We may also collect information that your browser sends whenever you
          visit our Service (“Log Data”). This Log Data may include information
          such as your computer’s Internet Protocol (“IP”) address, browser
          type, browser version, the pages of our Service that you visit, the
          time and date of your visit, the time spent on those pages and other
          statistics.
        </p>
        <p className="my8">
          In addition, we may use third party services such as Google Analytics
          that collect, monitor and analyze this type of information in order to
          increase our Service’s functionality. These third party service
          providers have their own privacy policies addressing how they use such
          information.
        </p>
        <h4 className="my16">Cookies</h4>
        <p className="my8">
          Cookies are files with small amount of data, which may include an
          anonymous unique identifier. Cookies are sent to your browser from a
          web site and stored on your computer’s hard drive.
        </p>
        <p className="my8">
          We use “cookies” to collect information. You can instruct your browser
          to refuse all cookies or to indicate when a cookie is being sent.
          However, if you do not accept cookies, you may not be able to use some
          portions of our Service. If you do not instruct your browser to refuse
          all cookies or to indicate when a cookie is being sent, your consent
          to our use of your Personal Information may be implied.
        </p>
        <h4 className="my16">Service Providers</h4>
        <p className="my8">
          We may employ third party companies and individuals to facilitate our
          Service, to provide the Service on our behalf, to perform
          Service-related services or to assist us in analyzing how our Service
          is used.
        </p>
        <p className="my8">
          These third parties have access to your Personal Information only to
          perform these tasks on our behalf and are obligated not to disclose or
          use it for any other purpose.
        </p>
        <h4 className="my16">Communications</h4>
        <p className="my8">
          We may use your Personal Information to contact you with newsletters,
          marketing or promotional materials and other information that may be
          of interest to you. You may opt out of receiving any, or all, of these
          communications from us by following the unsubscribe link or
          instructions provided in any email we send.
        </p>
        <h4 className="my16">Security</h4>
        <p className="my8">
          The security of your Personal Information is important to us, but
          remember that no method of transmission over the Internet, or method
          of electronic storage is 100% secure. While we strive to use
          commercially acceptable means to protect your Personal Information, we
          cannot guarantee its absolute security.
        </p>
        <h4 className="my16">International Transfer</h4>
        <p className="my8">
          Your information, including Personal Information, may be transferred
          to — and maintained on — computers located outside of your province,
          country or other governmental jurisdiction where the data protection
          laws may differ than those from your jurisdiction.
        </p>
        <p className="my8">
          If you are located outside Canada and choose to provide information to
          us, please note that we transfer the information, including Personal
          Information, to Canada and process it there.
        </p>
        <p className="my8">
          Your consent to this Privacy Policy followed by your submission of
          such information represents your agreement to that transfer.
        </p>
        <h4 className="my16">Links To Other Sites</h4>
        <p className="my8">
          Our Service may contain links to other sites that are not operated by
          us. If you click on a third party link, you will be directed to that
          third party’s site. We strongly advise you to review the Privacy
          Policy of every site you visit.
        </p>
        <p className="my8">
          We have no control over, and assume no responsibility for the content,
          privacy policies or practices of any third party sites or services.
        </p>
        <h4 className="my16">Children’s Privacy</h4>
        <p className="my8">
          Our Service does not address anyone under the age of 18 (“Minor”).
        </p>
        <p className="my8">
          We do not knowingly collect personally identifiable information from
          Minors. If you are a parent or guardian and you are aware that your
          Minor has provided us with Personal Information, please contact us. If
          we become aware that we have collected Personal Information from
          Minors without verification of parental consent, we take steps to
          remove that information from our servers.
        </p>
        <h4 className="my16">Changes To This Privacy Policy</h4>
        <p className="my8">
          We may update our Privacy Policy from time to time. We will notify you
          of any changes by posting the new Privacy Policy on this page.
        </p>
        <p className="my8">
          You are advised to review this Privacy Policy periodically for any
          changes. Changes to this Privacy Policy are effective when they are
          posted on this page.
        </p>
        <p className="my8">
          If we make any material changes to this Privacy Policy, we will notify
          you either through the email address you have provided us, or by
          placing a prominent notice on our website.
        </p>
        <h4 className="my16">Contact Us</h4>
        <p className="my8">
          If you have any questions about this Privacy Policy, please contact
          us.
        </p>
      </Page>
    );
  }
}

export default PrivacyPage;
